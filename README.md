ReClass.NET Unreal Plugin
=================================

A ReClass.NET plugin which displays type infos of Unreal Engine classes.
Based on UnrealPlugin https://github.com/ownbe/ReClass.NET-UnrealPlugin

Credits to KN4CK3R and Dr.Pepper.

![alt tag](https://abload.de/img/urealpluginmzj75.png)

SUPPORTED GAMES
---
- Sea of Thieves
- To get it work with other unreal games add a case with the processname of your game 
  and a signature for the GNames Array to the Code.

INSTALLATION
-----
- Clone from this repository and compile it yourself.
- Copy UnrealPlugin.dll in the appropriate plugin folder (ReClass.NET/x86/Plugins or ReClass.NET/x64/Plugins).
- Start ReClass.NET and check the plugins form.
