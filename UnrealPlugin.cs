﻿using ReClassNET.Extensions;
using ReClassNET.Memory;
using ReClassNET.MemoryScanner;
using ReClassNET.Nodes;
using ReClassNET.Plugins;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnrealPlugin
{
	public class UnrealPluginExt : Plugin
	{
		private IPluginHost host;
		public static IntPtr gNames;
		public static IntPtr gObjects;

		public override bool Initialize(IPluginHost host)
		{
			this.host = host;
			this.host.Process.ProcessAttached += OnProcessAttached;

			return true;
		}

		public override void Terminate()
		{
			this.host.Process.ProcessAttached -= OnProcessAttached;
			host = null;
		}

		public override IReadOnlyList<INodeInfoReader> GetNodeInfoReaders()
		{
			return new[] { new UnrealNodeInfoReader() };
		}

		private void OnProcessAttached(RemoteProcess process)
		{
			process.UpdateProcessInformations();

			var processName = System.IO.Path.GetFileName(process.UnderlayingProcess.Path).ToLower();
			switch (processName)
			{
				// TODO: Add more games

				case "sotgame.exe": // Sea of Thieves
					{
						var pattern = "48 8B 1D ? ? ? ? 48 85 ? 75 3A";
						var address = FindPattern(process, process.GetModuleByName(processName), pattern);

						if (!address.IsNull())
						{
							var offset = process.ReadRemoteInt32(address + 0x3);
							gNames = process.ReadRemoteIntPtr(address + offset + 7);
						}

						pattern = "48 8B 15 ? ? ? ? 3B 42 1C";
						address = FindPattern(process, process.GetModuleByName(processName), pattern);

						if (!address.IsNull())
						{
							var offset = process.ReadRemoteInt32(address + 0x3);
							gObjects = process.ReadRemoteIntPtr(address + offset + 7);
						}

						break;
					}
			}
		}

		private static IntPtr FindPattern(RemoteProcess process, Module module, string pattern)
		{
			var moduleBytes = process.ReadRemoteMemory(module.Start, module.Size.ToInt32());
			var bytePattern = BytePattern.Parse(pattern);

			var limit = moduleBytes.Length - bytePattern.Length;
			for (var i = 0; i < limit; ++i)
				if (bytePattern.Equals(moduleBytes, i))
					return module.Start + i;

			return IntPtr.Zero;
		}
	}

	public class UnrealNodeInfoReader : INodeInfoReader
	{
		public string ReadNodeInfo(BaseHexCommentNode node, IRemoteMemoryReader reader, MemoryBuffer memory, IntPtr nodeAddress, IntPtr nodeValue)
		{
			if (IsUObject(nodeValue, reader))
			{
				return GetUObjectName(nodeValue, reader);
			}

			return null;
		}

		private bool IsUObject(IntPtr objectPtr, IRemoteMemoryReader reader)
		{
			if (UnrealPluginExt.gObjects.IsNull())
				return false;

			var internalIndex = reader.ReadRemoteInt32(objectPtr + IntPtr.Size + 0x4);

			var numElements = reader.ReadRemoteInt32(UnrealPluginExt.gObjects + 0x10 + IntPtr.Size + 0x4);

			if (internalIndex < 0 || internalIndex >= numElements) return false;

			var objects = reader.ReadRemoteIntPtr(UnrealPluginExt.gObjects + 0x10);

#if RECLASSNET64
			var objectPtrCheck = reader.ReadRemoteIntPtr(objects + internalIndex * 0x18);
#else
			var objectPtrCheck = reader.ReadRemoteIntPtr(objects + internalIndex * 0x10);
#endif

			bool result = objectPtr == objectPtrCheck;

			return result;
		}

		private string GetUObjectName(IntPtr objectPtr, IRemoteMemoryReader reader)
		{
			if (UnrealPluginExt.gNames.IsNull())
			{
				return null;
			}

			var classObject = reader.ReadRemoteIntPtr(objectPtr + 0x10);

#if RECLASSNET64
			var nameIndex = reader.ReadRemoteInt32(classObject + 0x18);
#else
			var nameIndex = memory.Process.ReadRemoteInt32(objectPtr + 0x10);
#endif

			if (nameIndex < 1)
			{
				return null;
			}

			var numElements = reader.ReadRemoteInt32(UnrealPluginExt.gNames + 0x80 * IntPtr.Size);
			var numChunks = reader.ReadRemoteInt32(UnrealPluginExt.gNames + 0x80 * IntPtr.Size + 0x4);

			var indexChunk = nameIndex / 16384;
			var indexName = nameIndex % 16384;

			if (nameIndex < numElements && indexChunk < numChunks)
			{
				var chunkPtr = reader.ReadRemoteIntPtr(UnrealPluginExt.gNames + indexChunk * IntPtr.Size);

				if (chunkPtr.MayBeValid())
				{
					var namePtr = reader.ReadRemoteIntPtr(chunkPtr + indexName * IntPtr.Size);

					var nameEntryIndex = reader.ReadRemoteInt32(namePtr);

					if (nameEntryIndex >> 1 == nameIndex)
					{
						var wideChar = (nameEntryIndex & 1) != 0;

						var name = reader.ReadRemoteString(wideChar ? Encoding.Unicode : Encoding.ASCII, namePtr + 0x8 + IntPtr.Size, 1024);

						return name;
					}
				}
			}

			return null;
		}
	}
}
